# HTML-CSS-Interview-Exercise

HTML-CSS interview exercise for code interviews.

## Running the application

This is meant to be a super simple, static web page. Because of this, it is recommended to use `http-server`.

### Node

```bash
npx http-server . 8000
```

### Python 3.x

```bash
python -m http-server 8000
```

### Python 2.x

```bash
python -m SimpleHTTPServer
```
